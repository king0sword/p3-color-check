package com.a.b.c.poc

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class WidecolorEnabledActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        findViewById<TextView>(R.id.code).text = """
            <activity
                android:name=".WidecolorEnabledActivity"
                android:colorMode="wideColorGamut"/>
        """.trimIndent()
    }
}
